package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.domain.Company;
import com.thoughtworks.springbootemployee.domain.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class EmployeeRepository {
    static List<Employee> employees = new ArrayList<>();//全局可用

    public EmployeeRepository() {

    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }
    public Employee GetEmployeeById(int id) {
        return employees.stream()
                .filter(employee -> employee.getId() == id)
                .findFirst()
                .orElse(null);
    }
    public List<Employee> GetEmployeeByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }
    public int addEmployee(Employee employee) {
        employee.setId(nextId());
        employees.add(employee);
        return employee.getId();
    }
    private int nextId() {
        int currentMaxId = employees.stream().mapToInt(Employee::getId).max().orElse(0);
        return currentMaxId + 1;
    }
    public Employee updateEmployees(Integer id, Employee employee) {
        Employee updatedEmployee = employees.stream()
                .filter(employee2 -> employee2.getId().equals(id))
                .findFirst().orElse(null);
        if (updatedEmployee != null) {
//            BeanUtils.copyProperties(employee,updatedEmployee);
            updatedEmployee.setAge(employee.getAge());
            updatedEmployee.setSalary(employee.getSalary());
        }
        return updatedEmployee;
    }
    public Integer deleteEmployeeByEmployeeId(int employeeId) {
        employees.stream()
                .filter(employee1 -> employee1.getId().equals(employeeId))
                .findFirst()
                .ifPresent(employee -> employees.remove(employee));
        return employeeId;
    }
    public List<Employee> getPageCompany(int page, int size){
        return employees.stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }

    public List<Employee> getEmployeeByCompanyId(int companyId) {
            return employees.stream()
                    .filter(employee -> employee.getCompanyId()==companyId)
                    .collect(Collectors.toList());
    }

    public void deleteEmployeeByCompanyId(int deleteCompanyId) {
        employees.stream()
                .filter(employee -> employee.getCompanyId().equals(deleteCompanyId))
                .map(employee -> employees.remove(employee));
    }
}
