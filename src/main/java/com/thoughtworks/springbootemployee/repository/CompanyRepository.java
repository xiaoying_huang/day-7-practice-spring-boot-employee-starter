package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.domain.Company;
import com.thoughtworks.springbootemployee.domain.Employee;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyRepository {
    private List<Company> companies = new ArrayList<>();

    public CompanyRepository() {
    }

    public List<Company> getCompanies() {
        return companies;
    }

    public void setCompanies(List<Company> companies) {
        this.companies = companies;
    }
    public Company getCompanyById(int id){
        Company resultCompany = companies.stream().filter(company -> company.getId()
                        .equals(id))
                .findFirst()
                .orElse(null);
        return resultCompany;
    }
    public List<Company> getPageCompany(int page, int size){
        return companies.stream()
                .skip((long) (page - 1) *size)
                .limit(size)
                .collect(Collectors.toList());
    }
    public int addCompany(Company company) {
        company.setId(nextId());
        companies.add(company);
        return company.getId();
    }
    private int nextId() {
        int currentMaxId = companies.stream().mapToInt(Company::getId).max().orElse(0);
        return currentMaxId + 1;
    }
    public Company updateCompany(int id, Company company) {
        Company updatedCompany = companies.stream()
                .filter(company1 -> company1.getId().equals(id))
                .findFirst().orElse(null);
        if (updatedCompany != null) {
//            BeanUtils.copyProperties(employee,updatedCompany);
            updatedCompany.setName(company.getName());
        }
        return updatedCompany;
    }
    public Integer deleteCompany(int companyId) {
        companies.stream()
                .filter(company -> company.getId().equals(companyId))
                .findFirst()
                .ifPresent(company -> companies.remove(company));
        return companyId;
    }
}
