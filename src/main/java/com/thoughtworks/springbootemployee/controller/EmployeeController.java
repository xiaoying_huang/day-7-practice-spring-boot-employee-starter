package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.domain.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController //自动返回json数据
public class EmployeeController {
    private EmployeeRepository employeeRepository = new EmployeeRepository();

    public EmployeeController() {
        employeeRepository.getEmployees()
                .add(new Employee(1, "Lily", 20, "Female", 8000,1));
    }

    @GetMapping("/employees")
    public List<Employee> getAllEmployee() {
        return employeeRepository.getEmployees();
    }

    @GetMapping("/employees/{id}")
    public Employee getEmployeeById(@PathVariable int id) {
        return employeeRepository.GetEmployeeById(id);
    }

    @GetMapping(value = "/employees", params = "gender") //表示请求要带一个param
    public List<Employee> getEmployeeByGender(@RequestParam String gender) {
        return employeeRepository.GetEmployeeByGender(gender);
    }

    @PostMapping("/employees")
    @ResponseStatus(HttpStatus.CREATED)
    public int addEmployee(@RequestBody Employee employee) { //将json映射成类
        return employeeRepository.addEmployee(employee);
    }

    @PutMapping("/employees/{id}")
    public Employee updateEmployees(@PathVariable Integer id, @RequestBody Employee employee) {
        return employeeRepository.updateEmployees(id, employee);
    }

    @DeleteMapping("/employees/{id}")
    public Integer deleteEmployee(@PathVariable int id) {
        return employeeRepository.deleteEmployeeByEmployeeId(id);
    }

    @GetMapping(value = "/employees", params = {"page", "size"})
    public List<Employee> getPageEmployee(@RequestParam int page, @RequestParam int size){
        return employeeRepository.getPageCompany(page, size);
    }
}
