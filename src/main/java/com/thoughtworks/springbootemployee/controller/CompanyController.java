package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.domain.Company;
import com.thoughtworks.springbootemployee.domain.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository = new CompanyRepository();
    private final EmployeeRepository employeeRepository = new EmployeeRepository();

    public CompanyController() {
            companyRepository.getCompanies().add(new Company(1, "spring"));
    }
    @GetMapping
    public List<Company> getCompanies(){
        return companyRepository.getCompanies();
    }
    @GetMapping("/{id}")
    public Company getCompanyById(@PathVariable int id){
        return companyRepository.getCompanyById(id);
    }
    @GetMapping("/{id}/employees")//Error
    public List<Employee> getEmployeeByCompanyId(@PathVariable int id){
        return employeeRepository.getEmployeeByCompanyId(id);
    }
    @GetMapping(value = "", params = {"page", "size"})
    public List<Company> getPageCompany(@RequestParam int page, @RequestParam int size){
        return companyRepository.getPageCompany(page, size);
    }
    @PostMapping
    public int addCompany(@RequestBody Company company){
           return companyRepository.addCompany(company);
    }
    @PutMapping("/{id}")
    public Company updateCompany(@PathVariable int id, @RequestBody Company company){
        return companyRepository.updateCompany(id, company);
    }
    @DeleteMapping("/{companyId}")
    public List<Company> deleteCompany(@PathVariable int companyId){
        int deleteCompanyId =  companyRepository.deleteCompany(companyId);
        employeeRepository.deleteEmployeeByCompanyId(deleteCompanyId);
        return companyRepository.getCompanies();
    }
}
