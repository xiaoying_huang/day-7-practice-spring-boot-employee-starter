## Objective
- Today, I started learning Spring Boot and developing web applications using the Java language.
- I learned the basic concepts of Spring Boot, such as what it is and how it works, as well as how to create a Spring Boot application.
- I also learned about RESTful web services, HTTP methods, and how to use them to create APIs.

## Reflective
- At first, it's confused for me since I have none in Spring Boot.
- With the help of my partner, which is a new methods to program we learned today, I can code smoothly.

## Interpretive
- Spring Boot is more easier to understand when we practise more.

## Decisional
- Based on my learning today, I have decided to continue my study of Spring Boot and gain more knowledge about its advanced features. 
- I will also practice developing more web applications using Spring Boot to deepen my understanding of the framework and improve my skills.
 